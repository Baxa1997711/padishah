/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    images: {
        domains: ['media2.bulavka.uz', 'zoodmall.com', 'podishah.s3.amazonaws.com', 'images.wbstatic.net']
    }
}

module.exports = nextConfig
