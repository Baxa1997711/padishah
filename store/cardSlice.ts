import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    count: 0,
    orders: []
}

const cardSlice = createSlice(
    {
        name: "card",
        initialState,
        reducers: {
            increment: (state) => {
                state.count = state.count + 1;
            },
            addCard: (state, action) => {
                state.orders = [...state.orders, action.payload]
            }
        }
    }
)

export const {increment} = cardSlice.actions;

export default cardSlice.reducer;