import {useQuery} from "react-query";
import {request} from "../../services/Api";

export const useGetCategory = () => {
    const query = useQuery("use-get-all-category", async () => {
        const {data} = await request.get(`/category/categories-list`);
        return data;
    });
    return query;
};
