import {useQuery} from "react-query";
import {request} from "../../services/Api";

type ProductIdProps = {
    id: string | string[],
    enabled?: boolean

}

export const useGetProductById = ({id, enabled}: ProductIdProps) => {
        const query = useQuery("use-get-product-by-id", async () => {
            const {data} = await request.get(`/product/${id}`);
            return data;
        }, {enabled});
        return query;
    }
;
