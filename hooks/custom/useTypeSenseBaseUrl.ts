import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";

const useTypeSenseBaseUrl = (query_by: string) => {
    const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
        collectionSpecificSearchParameters: undefined,
        server: {
            apiKey: "HGD87yU2YTSGc56b",
            nodes: [
                {
                    host: process.env.API_HOST,
                    port: 8108,
                    protocol: process.env.API_PROTOCOL,
                },
            ],
            cacheSearchResultsForSeconds: 2 * 60, // Cache search results from server. Defaults to 2 minutes. Set to 0 to disable caching.
        },
        additionalSearchParameters: {
            // The following parameters are directly passed to Typesense's search API endpoint.
            //  So you can pass any parameters supported by the search endpoint below.
            //  queryBy is required.
            queryBy: query_by,
            // queryByWeights: '4,2,1',
            //numTypos: 1,
            typoTokensThreshold: 1,
            // groupBy: "categories",
            // groupLimit: 1
            // pinnedHits: "23:2"
        }
    });

    return typesenseInstantsearchAdapter.searchClient
}
export default useTypeSenseBaseUrl;