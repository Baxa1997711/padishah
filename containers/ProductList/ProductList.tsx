import React, {FC} from "react";
import {Col, Pagination, Row} from "antd";
import {Loader, ProductCard} from "../../components";
import {Hits} from 'react-instantsearch-dom'
import classes from "./product-list.module.scss";
import {useTypeSenseBaseUrl} from "../../hooks";
import Home from "../../pages/test-page";
import {findResultsState} from 'react-instantsearch-dom/server';
import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";
import {server} from "../../utils/constants";
import {Hit} from "../../components/Hit";

type products = {
    photos: string;
    photo: Array<string>;
    productId: number;
    description: string;
    price: number
}


interface ProductListProps {
    productList: Array<products>;
    searchClient?: any
    searchState?: any,
    resultsState?: any,
    onSearchParameters?: any,
    widgetsCollector?: any,
}


const ProductList: FC<ProductListProps> = ({productList}) => {
    // const searchClient = useTypeSenseBaseUrl("name, category");
    // const resultsState = findResultsState(Home, {
    //     indexName: "products",
    //     searchClient
    // });


    return (
        <div className={classes.product_list}>
            <Row gutter={[10, 10]}>
                {/*<Hits hitComponent={Hit}/>*/}
                {/*{products?.map(({slug, price, img, description}, index) => (*/}
                {/*<Col md={6} xs={12}>*/}
                <Hits hitComponent={ProductCard}/>
                {/*<ProductCard slug={slug} img={img} price={price} description={description}/>*/}
                {/*</Col>*/}
                {/*))}*/}
            </Row>
            <div className={classes.product_pagination} style={{textAlign: "center", }}>
                <Pagination defaultCurrent={1} total={500} showSizeChanger={false}/>
            </div>
        </div>
    )
}


export default ProductList;