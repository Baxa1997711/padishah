import React, {FC, useState} from "react";
import {EachOrder, Title} from "../../components";
import {useTranslation} from "react-i18next";
import {Card} from "antd";
import classes from "./basket.module.scss";

type Order = {
    color?: string;
    id: number;
    price: number;
    salePrice: number;
    size: number;
    productName: string;
    photo: string;
    quantity: number
}

interface BasketProps {
    order: Order[]
}

const Basket: FC<BasketProps> = ({order}) => {
    const {t} = useTranslation();
    const [quantity, setQuantity] = useState({quantity: 1, });


    return (
        <Card className={classes.basket_card}>
            <div className="d-flex justify-start">
                <Title className={classes.title} title={t("Корзина")}/>
                <sup className={classes.badge}>{order?.length}</sup>
            </div>
            {order?.map(({photo, id, size, color, salePrice, price, productName, quantity}) => (
                <EachOrder img={photo}
                           name={productName}
                           quantity={quantity}
                           key={id}
                           setQuantity={setQuantity}
                           price={price}
                           color={color}
                           salePrice={salePrice}
                           size={size}/>
            ))}
        </Card>
    )
}
export default Basket;