import React from "react"
import {Button, Dropdown, Menu} from "antd";
import {AppstoreOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import classes from "./seach-filter-section.module.scss";
import {AiOutlineAppstore} from "react-icons/ai";
import {TiThMenuOutline} from "react-icons/ti";


const SearchFilterSection = () => {
    const {t} = useTranslation();

    const menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: (
                        <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                            {t("По популярности")}
                        </a>
                    ),
                },
                {
                    key: '2',
                    label: (
                        <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                            {t("По рейтингу")}
                        </a>
                    ),
                },
                {
                    key: '3',
                    label: (
                        <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                            {t("По скидке")}
                        </a>
                    ),
                },
                {
                    key: '4',
                    label: (
                        <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                            {t("По возрастанию цены")}
                        </a>
                    ),
                },
                {
                    key: '5',
                    label: (
                        <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                            {t("По убыванию цены")}
                        </a>
                    ),
                },
            ]}
        />
    );

    return (
        <div className={classes.filter_section}>
            <Button className={classes.filter}>{t("Фильтр")}</Button>
            <div className={classes.change_view_section}>
                <Button className={classes.change_view_button} icon={<AiOutlineAppstore/>}/>
                <Button className={classes.change_view_button} icon={<TiThMenuOutline/>}/>
            </div>
            <Dropdown className={classes.sort_dropdown} overlay={menu} placement="bottom"
                      arrow={{pointAtCenter: true}}>
                <Button>{t("Сортировка")}</Button>
            </Dropdown>
        </div>
    )
}

export default SearchFilterSection;