import React from "react";
import classes from "./merchant-info.module.scss"
import {useTranslation} from "react-i18next";
import {Button} from "antd";

const MerchantInfo = () => {
    const {t} = useTranslation()

    return (
        <div className={classes.merchant_info}>
            <div>
                <h2>{t("Продавец")}</h2>
                <p className={classes.merchant_name}>Bambi</p>
                <p>Продано 100 шт</p>
            </div>
            <Button className={classes.connect_with_merchant}>{t("СВЯЗЬ С МАГАЗИНОМ")}</Button>
        </div>
    )
}

export default MerchantInfo;