import React from "react";
import classes from "./topSells.module.scss";
import {ProductCard} from "../../components";
import Image from "next/image";
import {Col, Row} from "antd";
import {Hits} from 'react-instantsearch-dom'

interface TopSellsSectionProps {
    products: Array<any>;
    title: string;
    brands?: Array<any>;
    pagination?: boolean;
}

const TopSells = ({products, title, brands, pagination = false}: TopSellsSectionProps) => {

    return (
        <div className={classes.top_sells}>
            <div className={classes.title}>
                <h2>{title}</h2>
                <div className={classes.border_bottom}/>
            </div>
            {brands &&
                <Row justify={"center"} gutter={[20, 20]} style={{marginBottom: 30}}>
                    {brands?.map((brand, index) => (
                        <Col md={2} key={index}>
                            <Image src={brand.img} width={80} height={80}/>
                        </Col>
                    ))}
                </Row>
            }
            <div className="row">
                <Hits hitComponent={ProductCard}/>
                {/*{products.map(({img, price, description, key, slug}) => (*/}
                {/*    <div className="col-5" key={key}>*/}
                {/*        */}
                {/*        <ProductCard {...{img, price, description, key, slug}}/>*/}
                {/*    </div>*/}
                {/*))}*/}
            </div>
            {/*{pagination && <div style={{textAlign: "center"}}>*/}
            {/*    <Pagination defaultCurrent={1} total={500} showSizeChanger={false}/>*/}
            {/*</div>}*/}
            {!!brands && <div style={{paddingBottom: 32}}/>}
        </div>
    )
}
export default TopSells;