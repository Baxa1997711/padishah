import React, {useState} from "react";
import classes from "./login.module.scss";
import {Input, Title} from "../../components";
import {Button, Checkbox, Col, message, Row} from "antd";
import {useTranslation} from "react-i18next";
import {useMutation} from "react-query";
import {request} from "../../services/Api";
import {useRouter} from "next/router";
import {useTimer} from 'react-timer-hook';
import {start} from "repl";

const LoginContainer = () => {
    const {t} = useTranslation();
    const router = useRouter();
    const [toPhoneNumber, setToPhoneNumber] = useState(true)
    const expiryTimestamp = new Date();
    expiryTimestamp.setSeconds(expiryTimestamp.getSeconds() + 59);
    const {seconds, minutes, start, restart} = useTimer({
        expiryTimestamp,
        autoStart: false,
        onExpire: () => setToPhoneNumber(true)
    });
    const [value, setValue] = useState(null);
    const [code, setCode] = useState(null);
    const useGetCode = useMutation((data: any) => request.post("/user/login", data), {
        onSuccess: (res) => {
            setToPhoneNumber(false);
            restart(expiryTimestamp);
            start();
        }
    })
    const useGetVerify = useMutation((data: any) => request.post("/user/verify", data), {
        onSuccess: (res) => {
            localStorage.setItem("token", res?.data?.accessToken);
            message.success(t("Войти успешно"));
            router.back();
        },
        onError: (err) => {
            console.log(err);
            message.error("Пожалуйста, попробуйте еще раз");
        },
    })


    const handleGetCode = (e) => {
        const phoneNum = "+998" + value;
        if (e === "login") {
            useGetVerify.mutate({msisdn: phoneNum, code})
        } else {
            useGetCode.mutate({msisdn: phoneNum})
        }
    }

    return (
        <div className={classes.login_container}>
            <Title className={classes.title} title={t("Войти или создать профиль")}/>
            <span className={classes.label}>{t("Контактный телефон")}</span>
            <Row gutter={[10, 10]}>
                <Col span={6}>
                    <Input className={classes.login_input} defaultValue={"+998"} field={"input"}/>
                </Col>
                <Col span={18}>
                    <Input className={classes.login_input} field={"format"} format={"## ### ## ##"} onValueChange={(e) => setValue(e?.floatValue)}
                           disabled={useGetCode?.isSuccess}/>
                </Col>
                {!toPhoneNumber && <Col span={24}>
                    <Input className={classes.login_input} onChange={(e) => setCode(e?.target?.value)}
                           placeholder={t("Код подтверждения")} field={"input"}/>
                </Col>}
                {!toPhoneNumber && <Col span={24}>
                    <Button loading={useGetVerify.isLoading} className={classes.get_code_button}
                            onClick={() => handleGetCode("login")}>
                        {t("Логин")}
                    </Button>
                </Col>}
                {toPhoneNumber && <Col span={24}>
                    <Button loading={useGetCode.isLoading} className={classes.get_code_button}
                            onClick={() => handleGetCode("code")}>
                        {t("Получить код")}
                    </Button>
                </Col>}
                {!toPhoneNumber && <Col span={24}>
                    <span>{t("Запросить код на телефон через")} 0{minutes}:{seconds}</span>
                </Col>}
                <Col span={24}>
                    <Checkbox checked={true} className={classes.terms_of_use}>
                        {t("Согласен с условиями")}
                        <a href="/assets/documents/Правила%20пользования.docx" download
                           style={{textDecoration: "underline", marginLeft: 5}}>
                            {t("Правил пользования")}
                        </a>
                    </Checkbox>
                </Col>
            </Row>
        </div>
    )
}

export default LoginContainer;