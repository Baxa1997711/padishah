import React, {FC, useEffect, useState} from "react";
import ProductCarousel from "../../components/ProductCarousel";
import {Button, Col, message, Row, Typography} from "antd";
import {useTranslation} from "react-i18next";
import classes from "./product-details.module.scss";
import {HeartOutlined, LeftOutlined, CheckOutlined} from "@ant-design/icons";
import {Modals} from "../../components";
import Slider from "react-slick";
import FormatNumber from "../../components/FormatNumber";
import {useRouter} from 'next/router'
import {MerchantInfo} from "../index";

const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 4,
    // prevArrow: <LeftOutlined/>,
    // nextArrow: <LeftOutlined/>
};

type Products = {
    productId: number;
    productName: string;
    colors: string;
    description?: string;
    salePrice?: number;
    price: number
    size: string;
    photos: string;
}

interface ProductDetailsProps {
    product: Products;
}

const ProductDetails: FC<ProductDetailsProps> = ({product}) => {
    const {Text, Title} = Typography;
    const [isModalVisible, setIsModalVisible] = useState(false);
    const router = useRouter()
    const {t} = useTranslation();
    const [checkSize, setCheckSize] = useState(null);
    const [checkColor, setCheckColor] = useState(null);
    const [order, setOrder] = useState([]);
    const [orderList, setOrderList] = useState([]);
    const {productId, productName, description, size, colors, photos, salePrice, price} = product || {};
    const photosList = JSON.parse(photos ? photos : "[]") || [];
    const sizesList = JSON.parse(size ? size : "[]") || [];
    const colorsList = JSON.parse(colors ? colors : "[]") || [];
    const isProductTaken = orderList?.some((item) => item?.id === productId && item?.size === checkSize && item?.color === checkColor)


    const handleColorSelect = (color) => {
        if (checkColor === color) {
            setCheckColor(null);
        } else {
            setCheckColor(color);
        }
    }

    const handleSizeSelect = (sz) => {
        if (checkSize === sz) {
            setCheckSize(null);
        } else {
            setCheckSize(sz)
        }
        // if (checkSize.includes(size)) {
        //     setCheckSize(checkSize.filter(item => item !== size))
        // } else {
        //     setCheckSize([...checkSize, size]);
        // }
    }

    const colorSelect = (color) => {
        switch (color) {
            case "#f5f5db":
            case "#ffffff":
            case "#add9e6":
            case "#808080":
                return "#000000"
            default:
                return "#FFFFFF"
        }
    }
    useEffect(() => {
        if (order?.length > 0) {
            localStorage.setItem("order", JSON.stringify(order));
        }
    }, [order])


    useEffect(() => {
        const ordersString = localStorage.getItem("order");
        const orders = JSON.parse(ordersString ? ordersString : "[]") || [];
        setOrderList(orders)
    }, [order])

    const handleAddToCard = () => {
        if (isProductTaken) {
            return router.push("/orders")
        }

        if (!checkSize || !checkColor) {
            return message.error("Пожалуйста, выберите цвет и размер!")
        }

        const AnOrder = {
            id: productId,
            productName,
            size: checkSize,
            photo: photosList?.[0],
            price,
            salePrice,
            color: checkColor,
            quantity: 1
        }
        // if (order?.some(item => item?.size !== checkSize && item?.color !== checkColor)) {
        //
        // }
        setOrder([...order, {...AnOrder}]);

    }


    return (
        <div className={classes.product_details}>
            <Row gutter={30}>
                <Col span={8}>
                    <ProductCarousel photosList={photosList} key={photosList?.[0]}/>
                </Col>
                <Col span={10}>
                    <h1 style={{marginTop: -5}}>
                        {productName}
                    </h1>
                    <div className={classes.price}>
                        <Text style={{marginRight: 16}}>{t("Цена")}:</Text>
                        <Text className={classes.sale_price}>
                            <FormatNumber num={salePrice} thousandSeparator={" "}/>
                        </Text>
                        <Text className={classes.sale_price}> {t("сўм")}</Text>
                        <del ><FormatNumber num={price} thousandSeparator={" "}/> сўм</del>
                    </div>
                    <div className={classes.colors}>
                        <Text style={{marginRight: 5}}>{t("Цвет")}:</Text>
                        <Text style={{color: "#384152"}}>черный</Text>
                        <div className={classes.color_list}>
                            {colorsList?.map((color) => (
                                <div onClick={() => handleColorSelect(color)}
                                     style={checkColor === color ? {
                                         backgroundColor: color,
                                         cursor: "pointer",
                                         border: "2px solid #F94001"
                                     } : {
                                         backgroundColor: color,
                                         cursor: "pointer",
                                     }} key={color}
                                     className={classes.color}>
                                    {/*{checkColor === color && <CheckOutlined*/}
                                    {/*    style={{color: colorSelect(color), fontSize: 20, fontWeight: 900}}/>}*/}
                                </div>
                            ))}
                        </div>
                    </div>
                    <Button type={"text"} onClick={() => setIsModalVisible(!isModalVisible)}
                            className={classes.sizes_table}>
                        {t("Таблица размеров")}
                    </Button>
                    <Modals key={"sizeModal"}
                            isModalVisible={isModalVisible}
                            setIsModalVisible={setIsModalVisible}
                            modalType={"size"}
                    />
                    <div className={classes.sizes}>
                        {sizesList?.map((size, index) => (
                            <Button onClick={() => handleSizeSelect(size)}
                                    style={checkSize === size ? {border: "2px solid #F94001", color: "#F94001"} : {}}
                                // icon={checkSize === size ? <CheckOutlined/> : null}
                                    className={classes.size_button}
                                    key={index}>{size}</Button>
                        ))}
                    </div>
                    <div style={{display: "flex", marginTop: 30}}>
                        <Button className={classes.buy_now} size={"large"}>{t("Купить сейчас")}</Button>
                        <Button className={classes.add_to_card} onClick={handleAddToCard}
                                size={"large"}>{isProductTaken ? t("Перейти в корзину") : t("Добавить в корзину")}</Button>
                    </div>
                </Col>
                <Col span={6}>
                    <MerchantInfo/>
                </Col>
            </Row>
            <Row>
                <Col span={10}>
                    <div className={classes.description_section}>
                        <Title className={classes.description_header}>{t("Описание")}</Title>
                        <p>{description}</p>
                    </div>
                    <div className={classes.description_section}>
                        <Title className={classes.description_header}>{t("Характеристика")}</Title>
                        <div>
                            <p>Цвет........................................................................................Черный</p>
                            <p>Вид........................................................................................Классический</p>
                            <p>Вид........................................................................................Классический</p>
                            <p>Вид........................................................................................Классический</p>
                            <p>Вид........................................................................................Классический</p>
                            <p>Вид........................................................................................Классический</p>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default ProductDetails;