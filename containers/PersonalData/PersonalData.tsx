import React from "react";
import {Button, Col, Form, Row} from "antd";
import Input from "../../components/Input";
import {useTranslation} from "react-i18next";
import classes from "./profile.module.scss";


const PersonalData = () => {
    const {t} = useTranslation();
    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <div style={{marginTop: 40}} className={classes.profile}>
            <Form
                name="basic"
                labelCol={{span: 24}}
                wrapperCol={{span: 24}}
                initialValues={{remember: true}}
                onFinish={onFinish}
                layout="vertical"
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Row>
                    <Col span={11}>
                        <Row gutter={[50, 0]}>
                            <Col span={12}>
                                <Form.Item
                                    label="Имя"
                                    name="username"
                                    // rules={[{required: true, message: 'Please input your username!'}]}
                                >
                                    <Input field="input"/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    label={t("Район")}
                                    name="district"
                                >
                                    <Input field="select"/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    label={t("Номер телефона")}
                                    name="phoneNumber"
                                >
                                    <Input field="input"/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    label={t("Адрес")}
                                    name="address"
                                >
                                    <Input field="input"/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    label={t("Регион")}
                                    name="region"
                                >
                                    <Input field="input"/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    label={t("Адрес 2")}
                                    name="address2"
                                >
                                    <Input field="input"/>
                                </Form.Item>
                            </Col>
                            <Col>
                                <Form.Item>
                                    <Button size="large"
                                            style={{
                                                backgroundColor: "#F94001",
                                                borderRadius: 5,
                                                fontWeight: 500,
                                                fontFamily: "Roboto",
                                                border: "none",
                                                marginTop: 10
                                            }}
                                            type="primary" htmlType="submit">
                                        {t("Сохранить изменения")}
                                    </Button>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}

export default PersonalData;