import React from "react";
import classes from "./deliver-address.module.scss";
import {useTranslation} from "react-i18next";
import {Button, Card, Form} from "antd";
import {Input, Title} from "../../components";

const layout = {
    labelCol: {span: 24},
    wrapperCol: {span: 18},
};

const DeliveryAddress = () => {
    const {t} = useTranslation();
    const [form] = Form.useForm();

    return (
        <Card className={classes.deliver_address}>

            <Title className={classes.title} title={t("Адрес доставки")}/>
            <Button className={classes.yandex_map_button}
                    style={{marginBottom: 14}}>{t("Указать адрес на карте")}</Button>
            <Form {...layout} form={form} requiredMark={false} layout="vertical" className={classes.form}>
                <Form.Item label={t("Имя")} rules={[{required: true, message: 'Обязательное поле!'}]}>
                    <Input style={{color: "#000", fontWeight: 500}} field={"input"} placeholder={t("Имя Фамилия")}/>
                </Form.Item>
                <Form.Item label={t("Номер телефона")} rules={[{required: true, message: 'Обязательное поле!'}]}>
                    <Input style={{color: "#000", fontWeight: 500}} field={"input"} prefix={"+998"}/>
                </Form.Item>
                <Form.Item label={t("Регион")} rules={[{required: true, message: 'Обязательное поле!'}]}>
                    <Input style={{color: "#000", fontWeight: 500}} field={"select"}/>
                </Form.Item>
                <Form.Item label={t("Район")}>
                    <Input style={{color: "#000", fontWeight: 500}} field={"select"}/>
                </Form.Item>
                <Form.Item label={t("Адрес")} rules={[{required: true, message: 'Обязательное поле!'}]}>
                    <Input style={{color: "#000", fontWeight: 500}} field={"input"}/>
                </Form.Item>
            </Form>
        </Card>
    )
}

export default DeliveryAddress;