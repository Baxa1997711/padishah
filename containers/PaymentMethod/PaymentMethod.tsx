import React, {useState} from "react";
import classes from "./payment-method.module.scss";
import {useTranslation} from "react-i18next";
import {Card, Col, Row} from "antd";
import Image from "next/image";
import {useWindowSize} from "../../utils/use-window-size";
import {DEFAULT_DESKTOP_WINDOW_SIZE} from "../../utils/constants";
import {Title} from "../../components";
import {HiCheckCircle} from "react-icons/hi";


const PaymentMethod = () => {
    const {t} = useTranslation();
    const {width} = useWindowSize();
    const [tick, setTick] = useState("-1")

    const orderStyles = (checkedOrder: string) => {
        return {
            width: width > DEFAULT_DESKTOP_WINDOW_SIZE ? 160 : 65,
            height: width > DEFAULT_DESKTOP_WINDOW_SIZE ? 102 : 40,
            border: tick === checkedOrder ? ".7px solid #F94001" : "1px solid #d1d1d1"
        }
    }


    return (
        <Card className={classes.payment_method}>
            <Title className={classes.title} title={t("Способ оплаты")}/>
            <Row gutter={5}>
                <Col lg={24} md={8}>
                    <div onClick={() => setTick("0")} style={orderStyles("0")} className={classes.payment_card}>
                        <p className="roboto-medium"
                           style={width < DEFAULT_DESKTOP_WINDOW_SIZE ? {fontSize: 8} : {}}>{t("Наличными при доставке")}</p>
                        {tick === "0" && <div className={classes.tick}>
                            <HiCheckCircle size={27}/>
                            {/*<RiCheckFill size={27}/>*/}
                        </div>}
                    </div>
                </Col>
                <Col lg={24} md={8}>
                    <div onClick={() => setTick("1")} style={orderStyles("1")} className={classes.payment_card}>
                        <img src={"/assets/icons/payme-card.svg"}/>
                        {tick === "1" && <div className={classes.tick}>
                            <HiCheckCircle size={27}/>
                            {/*<RiCheckFill size={27}/>*/}
                        </div>}
                    </div>
                </Col>
                <Col lg={24} md={8}>
                    <div onClick={() => setTick("2")} style={orderStyles("2")} className={classes.payment_card}>
                        <img src={"/assets/icons/click-card.svg"}/>
                        {tick === "2" && <div className={classes.tick}>
                            <HiCheckCircle size={27}/>
                            {/*<RiCheckFill size={27}/>*/}
                        </div>}
                    </div>
                </Col>
                <Col lg={24} md={8}>
                    <div onClick={() => setTick("3")} style={orderStyles("3")} className={classes.payment_card}>
                        <img src={"/assets/icons/apelsin-card.svg"}/>
                        {tick === "3" && <div className={classes.tick}>
                            <HiCheckCircle size={27}/>
                            {/*<RiCheckFill size={27}/>*/}
                        </div>}
                    </div>
                </Col>
            </Row>
        </Card>
    )
}
export default PaymentMethod;