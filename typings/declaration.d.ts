declare module '*.css' {
    import {CSSResult} from 'lit-element';
    const css: CSSResult;
    export default css;
}
declare module '*.scss' {
    const content: Record<string, string>;
    export default content;
}
