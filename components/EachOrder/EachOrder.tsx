import React, {FC, useEffect, useState} from "react";
import classes from "./each-order.module.scss";
import cn from "classnames";
import Image from "next/image";
import {Button, Typography} from "antd";
import {useTranslation} from "react-i18next";
import {MinusOutlined, PlusOutlined} from "@ant-design/icons";
import FormatNumber from "../FormatNumber";

interface EachOrderProps {
    img: string;
    name: string;
    color?: string;
    size?: number;
    quantity?: number;
    setQuantity: any;
    price: number;
    salePrice?: number;
}

const EachOrder: FC<EachOrderProps> = ({img, name, color, size, quantity, setQuantity, price, salePrice}) => {
    const {t} = useTranslation()
    const {Text} = Typography;
    const [order, setOrder] = useState();


    return (
        <div className={cn(classes.each_order, "d-flex justify-between")}>
            <div className={classes.product_details}>
                <div className={classes.image}>
                    <Image src={img} layout={"fill"}/>
                </div>
                <div className={classes.details}>
                    <p>{name}</p>
                    <div style={{marginTop: 7}}>
                        <Text className={classes.detail}>{t("Цвет")}: {color}</Text>
                        <br/>
                        <Text className={classes.detail}>{t("Размер")}: {size}</Text>
                    </div>
                </div>
            </div>
            <div className={classes.quantity}>
                <div>
                    <Button onClick={() => setQuantity(quantity > 0 ? quantity - 1 : 0)}
                            className={classes.button} icon={<MinusOutlined/>}/>
                    <span>{quantity}</span>
                    <Button onClick={() => setQuantity(quantity + 1)}
                            className={classes.button} icon={<PlusOutlined/>}/>
                </div>
                <Button className={classes.delete_button} style={{marginTop: 15}}  type="text">{t("удалить")}</Button>
            </div>
            <div className={classes.prices}>
                <p><FormatNumber num={salePrice ? salePrice : price}/> {t("сўм")}</p>
                {salePrice && <del>{price} {t("сўм")}</del>}
            </div>
        </div>
    )
}

export default EachOrder;