import React, {ReactNode} from "react";
import classes from "./category_card.module.scss"
import {Card} from "antd";

interface Props {
    img: string;
    title: string;
}

const CategoryCard = ({img, title}: Props) => {

    return (
        <>
            <Card hoverable className={classes.category_card} cover={<img style={{borderRadius: 10}} src={img} alt="images"/>}>
                <div className={classes.blur_card}>
                    <h2>{title}</h2>
                </div>
            </Card>
        </>
    )
}

export default CategoryCard;