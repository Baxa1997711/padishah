import React from "react";
import classes from "./title.module.scss";
import {Container} from "../index";
import cn from "classnames"
import {useWindowSize} from "../../utils/use-window-size";
import {DEFAULT_DESKTOP_WINDOW_SIZE} from "../../utils/constants";

type TitleProps = {
    title: string;
    className?: string
}

const Title = ({title, className}: TitleProps) => {
    const {width} = useWindowSize();

    return (
        <div className={cn(classes.title, className)}>
            <h1 style={width > DEFAULT_DESKTOP_WINDOW_SIZE ? {
                fontSize: 22,
                padding: "30px 0"
            } : {fontSize: 18, textAlign: "center"}}>{title}</h1>
        </div>
    )
}

export default Title;