import React from 'react';
import classes from "./productCard.module.scss"
import {Card} from "antd";
import Image from "next/image";
import NumberFormat from 'react-number-format';
import Link from "next/link";
import {CloseOutlined} from "@ant-design/icons"
import {useTranslation} from "react-i18next";

type ProductCardType = {
    closable?: boolean;
    hit: any
}

const ProductCard = ({closable, hit}: ProductCardType) => {
    const {t} = useTranslation();

    return (
        <Link href={`/products/${hit.product_id}`}>
            <Card
                hoverable
                // style={{width: 250}}
                extra={closable && <CloseOutlined style={{fontSize: 17}}/>}
                cover={<Image style={{objectFit: "contain"}} src={hit.image} alt="shoes" width={240} height={200}/>}
                className={classes.product_card}
                key={hit.product_id}
            >
                <div className={classes.card_footer}>
                    <p className={classes.description}>{hit.mini_desc}</p>
                    <p className={classes.price}><NumberFormat value={hit.price} displayType={'text'}
                                                               thousandSeparator={" "} suffix={t(" сум")}/>
                        {/*<span className={classes.currency} style={{fontWeight: "normal"}}> {t("сўм")}</span>*/}
                    </p>
                </div>
            </Card>
        </Link>

    )
}

export default ProductCard;