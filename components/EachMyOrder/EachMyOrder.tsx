import React, {FC} from "react";
import classes from "./each-my-order.module.scss";
import {useTranslation} from "react-i18next";
import {Button} from "antd";
import FormatNumber from "../FormatNumber";
import cn from "classnames";

interface EachOrderProps {
    index: number,
    orderNumber: number;
    orderDate: string;
    orderPrice: number;
    deliveryAddress: string;
    deliveryDate: string;
    paymentStatus: "payed" | "notPaid",
    orderStatus: string;

}

const EachMyOrder: FC<Partial<EachOrderProps>> = ({
                                                      index,
                                                      orderNumber,
                                                      orderDate,
                                                      orderPrice,
                                                      deliveryAddress,
                                                      deliveryDate,
                                                      paymentStatus,
                                                      orderStatus
                                                  }) => {
    const {t} = useTranslation();

    return (
        <div className={classes.each_my_order}>
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>{t("Номер заказа")}</th>
                    <th>{t("Дата заказа")}</th>
                    <th>{t("Сумма")}</th>
                    <th style={{width: 130}}>{t("Адрес доставки")}</th>
                    <th>{t("Дата доставки")}</th>
                    <th>{t("Статус")}</th>
                    <th>{t("Действие")}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{index}</td>
                    <td>{orderNumber}</td>
                    <td>{orderDate}</td>
                    <td><FormatNumber num={orderPrice}/> {t("сўм")}</td>
                    <td style={{width: 200}}>{deliveryAddress}</td>
                    <td>{deliveryDate}</td>
                    <td><Button disabled className={classes.action_buttons} style={{
                        backgroundColor: "#0ed901",
                        color: "white",
                        cursor: "default"
                    }}>{paymentStatus === "payed" ? t("Оплачено") : t("Не оплачено")}</Button></td>
                    <td><Button className={classes.action_buttons} disabled
                                style={{
                                    backgroundColor: "#f29603", color: "white",
                                    cursor: "default"
                                }}>{t("В обработке")}</Button></td>
                    <td><Button className={cn(classes.action_buttons, classes.cancel_button)}
                                style={{
                                    border: "1px solid #AAAAAA",
                                    color: "#AAAAAA",
                                }}>{t("Отменить заказ")}</Button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    )
}
export default EachMyOrder;