import React, {FC} from "react";
import {Breadcrumb as AntdBreadcrumb} from "antd"
import Link from "next/link";

type breadCrumb = {
    title: string;
    path?: string;
}
type Props = {
    breadCrumb: Array<breadCrumb>
    separator?: string;
    className?: string;
}

const Breadcrumb: FC<Props> = ({
                                   separator = "/",
                                   className,
                                   breadCrumb
                               }) => {
    return (
        <AntdBreadcrumb separator={separator} className={className}>
            {breadCrumb && breadCrumb.map((item, i) => (
                <AntdBreadcrumb.Item key={i}>
                    {item.path
                        ? (<Link href={item.path}>{item.title}</Link>)
                        : (item.title)
                    }
                </AntdBreadcrumb.Item>
            ))}
        </AntdBreadcrumb>
    );
}
export default Breadcrumb;