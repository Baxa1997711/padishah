import React from "react";
import {InstagramOutlined} from "@ant-design/icons";
import classes from "./footer.module.scss";
import {useWindowSize} from "../../utils/use-window-size";
import {useTranslation} from "react-i18next";
import cn from "classnames";
import {Col, Collapse, Row} from "antd";
import {DEFAULT_MOBILE_WINDOW_SIZE} from "../../utils/constants";

const Footer = () => {
    const {width} = useWindowSize();
    const {t} = useTranslation();
    const {Panel} = Collapse;

    return (
        <>
            {width > DEFAULT_MOBILE_WINDOW_SIZE ? <div className={width > 768 ? "container" : "mini_container"}>
                    <Row gutter={10} justify={"space-between"} className="align-start">
                        <Col span={5} className={classes.columns}>
                            <p className={classes.column_header}
                               style={{textTransform: "uppercase", paddingBottom: 16}}>{t("компания")}</p>
                            <a href="#">{t("О нас")}</a>
                            <a href="#">{t("Пользовательское соглашение")}</a>
                        </Col>
                        <Col span={5} className={classes.columns}>
                            <p className={classes.column_header}
                               style={{textTransform: "uppercase", paddingBottom: 16}}>{t("помощь")}</p>
                            <a href="#">{t("Часто задаваемые вопросы")}</a>
                            <a href="#">{t("Обратный звонок")}</a>
                        </Col>
                        <Col span={6} className={classes.columns}>
                            <p className={classes.column_header}
                               style={{textTransform: "uppercase", paddingBottom: 16}}>{t("как пользоваться")}?</p>
                            <a href="#">{t("Оплата и доставка")}</a>
                            <a href="#">{t("Возврат товара и денежных средств")}</a>
                        </Col>
                        <Col span={4} className={classes.columns}>
                            <p className={classes.column_header}
                               style={{textTransform: "uppercase", paddingBottom: 16}}>{t("Контакты")}</p>
                            <a href="#">+99899 555 44 33</a>
                            <a href="#">support@padishah.uz</a>
                        </Col>
                        <Col span={4} className={classes.columns}>
                            <p className={classes.column_header}
                               style={{textTransform: "uppercase", paddingBottom: 16}}>{t("Социальные сети")}</p>
                            <div className="d-flex justify-start">
                                <div style={{marginRight: 10}}>
                                    <a href="#"><img src="/assets/icons/facebook.svg" alt=""/></a>
                                </div>
                                <div>
                                    <a href="#"><img src="/assets/icons/instagram.svg" alt=""/></a>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    {/*<div className="row text-left align-start justify-between">*/}
                    {/*    <div className={cn("col-5", classes.columns)}>*/}
                    {/*        <a className={classes.column_header} href="#"*/}
                    {/*           style={{textTransform: "uppercase", paddingBottom: 16}}>{t("компания")}</a>*/}
                    {/*        <a href="#">{t("О нас")}</a>*/}
                    {/*        <a href="#">{t("Пользовательское соглашение")}</a>*/}
                    {/*    </div>*/}
                    {/*    <div className={cn("col-5", classes.columns)}>*/}
                    {/*        <a className={classes.column_header} href="#"*/}
                    {/*           style={{textTransform: "uppercase", paddingBottom: 16}}>{t("помощь")}</a>*/}
                    {/*        <a href="#">{t("Часто задаваемые вопросы")}</a>*/}
                    {/*        <a href="#">{t("Обратный звонок")}</a>*/}
                    {/*    </div>*/}
                    {/*    <div className={cn("col-5", classes.columns)}>*/}
                    {/*        <a className={classes.column_header} href="#"*/}
                    {/*           style={{textTransform: "uppercase", paddingBottom: 16}}>{t("как пользоваться")}?</a>*/}
                    {/*        <a href="#">{t("Оплата и доставка")}</a>*/}
                    {/*        <a href="#">{t("Возврат товара и денежных средств")}</a>*/}
                    {/*    </div>*/}
                    {/*    <div className={cn("col-5", classes.columns)}>*/}
                    {/*        <a className={classes.column_header} href="#"*/}
                    {/*           style={{textTransform: "uppercase", paddingBottom: 16}}>{t("Контакты")}</a>*/}
                    {/*        <a href="#">+99899 555 44 33</a>*/}
                    {/*        <a href="#">support@padishah.uz</a>*/}
                    {/*    </div>*/}
                    {/*    <div className={cn("col-5 text-right", classes.columns)}>*/}
                    {/*        <a className={classes.column_header} href="#"*/}
                    {/*           style={{textTransform: "uppercase", paddingBottom: 16}}>{t("Социальные сети")}</a>*/}
                    {/*        <div className="d-flex justify-start">*/}
                    {/*            <div style={{marginRight: 10}}>*/}
                    {/*                <a href="#"><img src="/assets/icons/facebook.svg" alt=""/></a>*/}
                    {/*            </div>*/}
                    {/*            <div>*/}
                    {/*                <a href="#"><img src="/assets/icons/instagram.svg" alt=""/></a>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <div style={{marginTop: 70}} className="d-flex justify-between">
                        <p className="roboto-medium">&copy; 2021-2022 Padishah Platforms Ltd, All Rights Reserved</p>
                        <div className={classes.payments}>
                            <img src="/assets/icons/payme.svg" alt="payme"/>
                            <img src="/assets/icons/click.svg" alt="click"/>
                            <img src="/assets/icons/apelsin.svg" alt="Apelsin"/>
                        </div>
                    </div>
                </div>
                :
                <div className="container">
                    <Collapse
                        bordered={false}
                        style={{backgroundColor: "transparent", color: "#FFFFFF"}}
                        expandIconPosition={"right"}
                        className={classes.footer_collapse}
                    >
                        <Panel header={t("КОМПАНИЯ")} key="1">
                            <a href="#">{t("О нас")}</a>
                            <a href="#">{t("Пользовательское соглашение")}</a>
                        </Panel>
                        <Panel header={t("ПОМОЩЬ")} key="2">
                            <a href="#">{t("Часто задаваемые вопросы")}</a>
                            <a href="#">{t("Обратный звонок")}</a>
                        </Panel>
                        <Panel header={t("КАК ПОЛЬЗОВАТЬСЯ") + "?"} key="3">
                            <a href="#">{t("Оплата и доставка")}</a>
                            <a href="#">{t("Возврат товара и денежных средств")}</a>
                        </Panel>
                        <Panel header={t("Контакты")} key="4">
                            <a href="#">+99899 555 44 33</a>
                            <a href="#">support@padishah.uz</a>
                        </Panel>
                    </Collapse>
                    <div className={cn(classes.columns)}>
                        <div className="d-flex justify-start">
                            <div style={{marginRight: 10}}>
                                <a href="#"><img src="/assets/icons/facebook.svg" alt=""/></a>
                            </div>
                            <div>
                                <a href="#"><img src="/assets/icons/instagram.svg" alt=""/></a>
                            </div>
                        </div>
                    </div>
                    <Row className="align-end" justify={"space-between"} style={{marginTop: 25}}>
                        <Col span={12}>
                            <p style={{marginBottom: 0}} className="roboto-medium">&copy; 2021-2022 Padishah Platforms Ltd, All Rights
                                Reserved</p>
                        </Col>
                        <Col span={11}>
                            <div>
                                <img style={{marginRight: 10}} width={45} src="/assets/icons/payme.svg" alt="payme"/>
                                <img style={{marginRight: 10}} width={45} src="/assets/icons/click.svg" alt="click"/>
                                <img width={45} src="/assets/icons/apelsin.svg" alt="Apelsin"/>
                            </div>
                        </Col>
                    </Row>
                </div>
            }


        </>


    )
}
export default Footer;