import {Button, Checkbox, Col, Row} from "antd";
import React, {FC, useState} from "react";
import {CheckboxValueType} from "antd/lib/checkbox/Group";
import {useTranslation} from "react-i18next";
import {connectRefinementList} from "react-instantsearch-dom"
import classes from "./checkbox-group.module.scss";

type checkboxStates = {
    span: number,
    label: string
    value: string | number,
}

type CheckBoxGroupProps = {
    checkBoxData: Array<checkboxStates>
    filterLength?: number;
    items?: any;
    refine?: any;
}

const CheckBoxGroup: FC<CheckBoxGroupProps> = ({items, refine}) => {
    const {t} = useTranslation()
    console.log(items, 199);

    const onChange = (checkedValues: CheckboxValueType[]) => {
        console.log('checked = ', checkedValues);
    };

    return (
        <div className={classes.checkbox_group}>
            <Checkbox.Group style={{width: '100%'}} onChange={onChange}>
                <Row gutter={[20, 5]}>
                    {items?.map(({span, label, value}, index) => (
                        <Col span={span} key={index}>
                            <Checkbox value={value} onChange={
                                (event: any) => {
                                    // event.preventDefault();
                                    refine(value);
                                }
                            } style={{
                                fontFamily: "Roboto R, sans-serif",
                                fontSize: 16,
                            }}>{label}</Checkbox>
                        </Col>
                    ))}
                </Row>
                {/*{(checkBoxData?.length - filterLength) > 0 &&*/}
                {/*    <Button style={{float: "right", marginTop: 10, color: "#F94001", fontWeight: "bold"}}*/}
                {/*            onClick={() => setMore(!isMore)}*/}
                {/*            type="text">{isMore ? "-" : "+"}{checkBoxData?.length - filterLength} {isMore ? t("less") : t("more")}</Button>}*/}
            </Checkbox.Group>
        </div>
    )
}

export const CustomRefinementList = connectRefinementList(CheckBoxGroup)