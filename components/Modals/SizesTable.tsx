import React from "react";
import {Button, Table} from "antd";
import Title from "../Title";
import {useTranslation} from "react-i18next";
import {ColumnsType} from "antd/lib/table/interface";
import classes from "./modals.module.scss"

const SizesTable = () => {
    const {t} = useTranslation();

    const dataSource = [
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
        {
            euroStandard: '32',
            uzStandard: 32,
            footsLength: '32',
        },
    ];

    const columns: ColumnsType = [
        {
            title: t("Евро стандарт"),
            dataIndex: 'euroStandard',
            key: 'euroStandard',
            align: "center"
        },
        {
            title: t("Уз стандарт"),
            dataIndex: 'uzStandard',
            key: 'uzStandard',
            align: "center"
        },
        {
            title: t("Длина стопы в см."),
            dataIndex: 'footsLength',
            key: 'footsLength',
            align: "center"
        },
    ];


    return (
        <div>
            <Title title={t("Таблица размеров")}/>
            <Table className={classes.sizes_table} dataSource={dataSource} columns={columns} pagination={false}/>
            <Button style={{marginTop: 20, borderRadius: 5}}>{t("Выбрать размер")}</Button>
        </div>
    )
}

export default SizesTable;