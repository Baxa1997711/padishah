import React, {FC, useState} from "react";
import {Modal} from "antd";
import SizesTable from "./SizesTable";

interface ModalTypes {
    isModalVisible: boolean
    setIsModalVisible: (isModalVisible: boolean) => void;
    modalType: "size" | "image"
}


const Modals: FC<ModalTypes> = ({isModalVisible, setIsModalVisible, modalType, ...props}) => {


    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <Modal width={1124} title={null} footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <SizesTable/>
        </Modal>
    )
}

export default Modals;