import React from "react";
import Header from "./Header/Header";
import classes from "./layout.module.scss"
import {Container} from "../index";

const Layout = ({children}) => {
    return (
        <div className={classes.layout}>
            <Header/>
            {children}
        </div>
    )
}
export default Layout;