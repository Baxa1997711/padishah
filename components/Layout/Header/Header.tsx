import React, {useEffect, useState} from "react";
import classes from "./header.module.scss";
import {Badge, Button} from "antd";
import {SearchOutlined, UserOutlined} from "@ant-design/icons";
import Link from "next/link";
import Input from "../../Input";
import MenuDrawer from "./MenuDrawer";
import cn from "classnames";
import {useWindowSize} from "../../../utils/use-window-size";
import {DEFAULT_DESKTOP_WINDOW_SIZE, DEFAULT_MOBILE_WINDOW_SIZE} from "../../../utils/constants";
import {useTranslation} from "react-i18next";
import {InstantSearch, useSearchBox} from "react-instantsearch-dom"
import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";
import {assembleTypesenseServerConfig} from "../../../utils/assembleTypesenseServerConfig";
import {findResultsState} from "react-instantsearch-dom/server";
import Search from '../../../public/assets/icons/search.svg'

const TYPESENSE_SERVER_CONFIG = assembleTypesenseServerConfig();


const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
    server: TYPESENSE_SERVER_CONFIG,
    additionalSearchParameters: {
        // The following parameters are directly passed to Typesense's search API endpoint.
        //  So you can pass any parameters supported by the search endpoint below.
        //  queryBy is required.
        query_by: 'name,category',
        // queryByWeights: '4,2,1',
        // numTypos: 1,
        typoTokensThreshold: 1,
        // groupBy: "categories",
        // groupLimit: 1
        // pinnedHits: "23:2"
    },
});


const Header = ({
                    searchClient = typesenseInstantsearchAdapter.searchClient,
                }) => {
    const [visible, setVisible] = useState(false);
    const {width} = useWindowSize();
    const {t} = useTranslation();
    const logoWidth = width > DEFAULT_MOBILE_WINDOW_SIZE ? 165 : 81;
    const logoHeight = width > DEFAULT_MOBILE_WINDOW_SIZE ? 37 : 19;


    return (
        <InstantSearch indexName="products"
                       searchClient={searchClient}
        >
            <div >
                <div
                    className={"container"}
                >
                    <div
                        className={cn(classes.header)}>
                        <div className="d-flex justify-start">
                            <div className={classes.user} style={{padding: 0}} onClick={() => setVisible(true)}>
                                <img src="/assets/icons/menu.svg" alt="menu icon"/>
                                <span>{t("МЕНЮ")}</span>
                            </div>
                            <div className={classes.padishah}>
                                <Link href="/">
                                    <img width={logoWidth} height={logoHeight} src="/assets/icons/padishah.svg"
                                         alt="padishah"/>
                                </Link>
                            </div>
                        </div>
                        <div className={classes.search_section}>
                            <Input field="input" className={classes.search_input} size={"large"} placeholder="Поиск..."
                                   prefix={<SearchOutlined style={{color: "#9CA3AF", marginRight: 4}}/>}/>
                        </div>
                        <div>
                            <div className="d-flex">
                                
                                <Link href={"/login"}>
                                    <div className={classes.user}>
                                        <img src="/assets/icons/user_icon.svg" alt="user icon"/>
                                        <span style={{marginTop: 2}}>{t("ВХОД")}</span>
                                    </div>
                                </Link>
                                <Link href={"/orders"}>
                                    <div className={classes.user} style={{paddingRight: 0}}>
                                        {/*<Badge count={0}>*/}
                                        <img src="/assets/icons/basket.svg" alt="basket icon"/>
                                        <span>{t("КОРЗИНКА")}</span>
                                        {/*</Badge>*/}
                                    </div>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className={classes.mobile_search_section}>
                            <Input field="input" className={classes.search_input} size={"large"} placeholder="Поиск..."
                                   />
                            <button className={classes.search_btn}>
                                <img src="/assets/icons/search.svg" className={classes.search_img} alt="" />
                            </button>       
                        </div>
                    {/*{width < DEFAULT_DESKTOP_WINDOW_SIZE && <div className={classes.search_section}>*/}
                    {/*    <Input field="input" className={classes.search_input} size={"large"} placeholder="Поиск..."*/}
                    {/*           prefix={<SearchOutlined style={{color: "#9CA3AF"}}/>}/>*/}
                    {/*</div>}*/}
                    <MenuDrawer setVisible={setVisible} visible={visible}/>
                </div>
            </div>
        </InstantSearch>
    );
}

export default Header;