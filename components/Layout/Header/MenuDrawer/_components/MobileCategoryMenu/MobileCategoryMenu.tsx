import React, {Dispatch, SetStateAction, useState} from "react";
import {Drawer, Menu, MenuProps} from "antd";
import classes from "../category_menu.module.scss";
import {CloseOutlined, YuqueOutlined} from "@ant-design/icons";
import CategoryMenu from "../CategoryMenu";
import {ArrowLeftOutlined} from "@ant-design/icons";

const category = [
    {
        key: "1",
        icon: <YuqueOutlined/>,
        label: "Женская обувь",
    },
    {
        key: "2",
        icon: <YuqueOutlined/>,
        label: "Мужская обувь",
    },
    {
        key: "3",
        icon: <YuqueOutlined/>,
        label: "Детская обувь",
    },
    {
        key: "4",
        icon: <YuqueOutlined/>,
        label: "Красота",
    },
];
const subcategory = [
    {
        key: "1",
        label: "Женская обувь",
    },
    {
        key: "2",
        label: "Женская обувь",
    },
    {
        key: "3",
        label: "Женская обувь",
    },
    {
        key: "4",
        label: "Женская обувь",
    },
];

type MobileCategoryMenuProps = {
    items: Array<{
        key: string,
        icon?: any,
        label: string,
    }>
    setVisible?: Dispatch<SetStateAction<boolean>>;
    visible?: boolean;
}
type SubcategoryDrawerProps = {
    subcategoryDrawerVisible: boolean,
    setSubcategoryDrawerVisible: Dispatch<SetStateAction<boolean>>;
    visible?: Boolean,
    setVisible?: Dispatch<SetStateAction<boolean>>;
}

const MobileCategoryMenu = ({items, visible, setVisible}: MobileCategoryMenuProps) => {

    const [subcategoryDrawerVisible, setSubcategoryDrawerVisible] = useState(false)
    const onClick: MenuProps['onClick'] = e => {
        visible !== undefined && setVisible(false)
        visible !== undefined && setSubcategoryDrawerVisible(true)
        // console.log('click ', e);
    };

    return (
        <div>
            <Menu
                onClick={onClick}
                style={{width: 220}}
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                mode="inline"
                items={items}
                className={classes.menu}
            />
            <SubcategoryDrawer {...{subcategoryDrawerVisible, setSubcategoryDrawerVisible, visible, setVisible}}/>
        </div>
    )
}

const SubcategoryDrawer = ({
                               subcategoryDrawerVisible,
                               setSubcategoryDrawerVisible,
                               visible,
                               setVisible
                           }: SubcategoryDrawerProps) => {

    const handleSubcategoryDrawerClose = () => {
        setSubcategoryDrawerVisible(false)
    }
    const handleBackDrawer = () => {
        setTimeout(() => {
            setVisible(true)
        }, 200)
        setSubcategoryDrawerVisible(false)
    }

    return (
        <Drawer
            title={<div onClick={handleBackDrawer} style={{cursor: "pointer"}}>
                <ArrowLeftOutlined/>
            </div>}
            placement={"left"}
            width={320}
            closable={false}
            onClose={handleSubcategoryDrawerClose}
            visible={subcategoryDrawerVisible}
            key={"left"}
            extra={<div onClick={handleSubcategoryDrawerClose} style={{cursor: "pointer"}}>
                <CloseOutlined/>
            </div>}
        >
            <MobileCategoryMenu items={subcategory}/>
        </Drawer>
    )
}

export default MobileCategoryMenu;