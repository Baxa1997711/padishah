import React, {useState} from "react";
import {Col, Menu, MenuProps, Row, Spin} from "antd";
import Image from "next/image";
import {YuqueOutlined} from "@ant-design/icons";
import classes from "../category_menu.module.scss";
import {useGetCategory} from "hooks/query";
import {useGetSubcategory} from "hooks/query/useGetSubcategory";


const CategoryMenu = () => {
    const categoryData = useGetCategory();
    const subcategoryData = useGetSubcategory();
    const [categoryid, setCategoryId] = useState(categoryData?.data?.[0]?.categoryId)
    const categories = categoryData?.data?.map(({categoryId, icon, name}) => ({
        key: categoryId,
        icon: <Image width={20} height={20} src={icon}/>,
        label: <span style={{marginLeft: 5}}>{name}</span>
    }));
    const subcategories = subcategoryData?.data?.filter(item => item?.categoryId == categoryid)?.map(({
    name,
    subCategoryId
}) => {
return {
            label: name,
            key: subCategoryId
        }
    })
    const onClick: MenuProps['onClick'] = e => {
        setCategoryId(e?.key);
    };

    if (categories === undefined && subcategories === undefined) {
        return <Spin size="large"/>
    }

    return (
        <div style={{height: "100%"}}>
            <Row gutter={20} style={{height: "100%"}}>
                <Col span={12} style={{borderRight: "1px solid #D8D8D8"}}>
                    <Menu
                        onClick={onClick}
                        style={{width: 220}}
                        defaultSelectedKeys={categories?.[0]?.key}
                        selectedKeys={categories?.[0]?.key}
                        mode="inline"
                        items={categories}
                        className={classes.menu}
                    />
                </Col>
                <Col span={12}>
                    <Menu
                        onClick={onClick}
                        style={{width: 256}}
                        defaultSelectedKeys={subcategories?.[0]?.key}
                        mode="inline"
                        items={subcategories}
                        className={classes.submenu}
                    />
                </Col>
            </Row>
        </div>
    )
}

export default CategoryMenu;