import React, {useState} from "react";
import {Button} from "antd";
import {useTranslation} from "react-i18next";
import {HeartOutlined} from "@ant-design/icons";
import {MyOrders, TabButtons} from "../containers";
import {useRouter} from "next/router";
import {Favorites, PersonalData} from "../components";

const Profile = () => {
    const {t} = useTranslation();
    const router = useRouter();
    const activeTabs = router?.query?.activeTabs

    return (
        <div className="container">
            <TabButtons/>
            {activeTabs === "0" && <PersonalData/>}
            {activeTabs === "1" && <MyOrders/>}
            {activeTabs === "2" && <Favorites/>}
        </div>
    )
}

export default Profile;