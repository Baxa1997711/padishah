import React, {useState} from "react";
import {useRouter} from "next/router";
import {Breadcrumb, Container, Loader, ProductCard, Modals} from "../../components";
import {useTranslation} from "react-i18next";
import {Button, Typography} from "antd";
import {ArrowLeftOutlined} from "@ant-design/icons";
import {ProductDetails} from "../../containers";
import {useGetProductById} from "../../hooks/query";
import Slider from "react-slick";
import classes from "../../containers/ProductDetails/product-details.module.scss";


const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 4,
    // prevArrow: <LeftOutlined/>,
    // nextArrow: <LeftOutlined/>
};

const EachProduct = () => {
    const {t} = useTranslation();
    const router = useRouter();
    const productById = useGetProductById({id: router?.query?.slug, enabled: !!router?.query?.slug});
    const product = productById?.data;
    const {Title} = Typography;

    const data = {
        productId: 1,
        productName: "Nike Yeezy running shoes best choice for sport everyday",
        description: "thhh hteer dhasbdh hbasdvy dhaey habsd hasbd hsavd",
        size: "[12,23,43,23,43,44,45,46]",
        colors: '["black", "white", "yellow", "blue", "#F94001", "#004242"]',
        photos: '["/assets/each_shoes.png","/assets/each_shoes.png","/assets/each_shoes.png","/assets/each_shoes.png","/assets/each_shoes.png","/assets/each_shoes.png"]',
        salePrice: 2323,
        price: 3232
    }

    const breadcrumb = [
        {
            title: t("Главная"),
            path: "/"
        },
        {
            title: t("Мужская обувь"),
            path: "/search"
        },
        {
            title: t("Кроссовки"),
        }
    ];

    // if (productById.isLoading) {
    //     return <div className="d-flex justify-center">
    //         <Loader/>
    //     </div>
    // }


    return (
        <Container className="container">
            <div className="d-flex" style={{padding: "20px 0"}}>
                <Button icon={<ArrowLeftOutlined/>} style={{
                    border: "1px solid #374151",
                    borderRadius: 5,
                    marginRight: 15
                }} onClick={() => router.back()}>{t("Назад")}</Button>
                <Breadcrumb breadCrumb={breadcrumb}/>
            </div>
            <ProductDetails product={data} key={product?.product}/>
            <div className={classes.description_section}>
                <Title style={{fontSize: 22}} className={classes.description_header}>{t("Похожие товары")}</Title>
                <div style={{background: "#374151", borderRadius: 100, width: 88, height: 4}}/>
                <div style={{padding: "20px 0"}}>
                    <Slider {...settings}>
                        {/*<div>*/}
                        {/*    <ProductCard slug={2321} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div style={{marginRight: 10}}>*/}
                        {/*    <ProductCard slug={6771} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                        {/*<div>*/}
                        {/*    <ProductCard slug={990} img={"/assets/shoes.png"}*/}
                        {/*                 description={"Олма ота поябзаллари"}*/}
                        {/*                 price={650000}/>*/}
                        {/*</div>*/}
                    </Slider>
                </div>
            </div>
        </Container>
    )
}

export default EachProduct;