import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";

export const colors = [
    {
        span: 12,
        label: 'бежевый',
        value: '#f5f5db'
    },
    {
        span: 12,
        label: 'белый',
        value: '#ffffff'
    },
    {
        span: 12,
        label: 'голубой',
        value: '#add9e6'
    },
    {
        span: 12,
        label: 'желтый',
        value: '#ffff00'
    },
    {
        span: 12,
        label: 'зеленый',
        value: '#008001'
    },
    {
        span: 12,
        label: 'коричневый',
        value: '#a52b2a'
    },
    {
        span: 12,
        label: 'красный',
        value: '#fe0000'
    },
    {
        span: 12,
        label: 'оранжевый',
        value: '#ffa300'
    },
    {
        span: 12,
        label: 'розовый',
        value: '#ffc0cb'
    },
    {
        span: 12,
        label: 'серый',
        value: '#808080'
    },
    {
        span: 12,
        label: 'синий',
        value: '#0000ff'
    },
    {
        span: 12,
        label: 'фиолетовый',
        value: '#8B00FF'
    },
    {
        span: 12,
        label: 'черный',
        value: '#000000'
    },
];
export const sizes = [
    {
        span: 8,
        label: "19",
        value: 19
    },
    {
        span: 8,
        label: "19.5",
        value: 19.5
    },
    {
        span: 8,
        label: "20",
        value: 20
    },
    {
        span: 8,
        label: "20.5",
        value: 20.5
    },
    {
        span: 8,
        label: "21",
        value: 21
    },
    {
        span: 8,
        label: "21.5",
        value: 21.5
    },
    {
        span: 8,
        label: "22",
        value: 22
    },
    {
        span: 8,
        label: "22.5",
        value: 22.5
    },
    {
        span: 8,
        label: "23",
        value: 23
    },
    {
        span: 8,
        label: "23.5",
        value: 23.5
    },
    {
        span: 8,
        label: "24",
        value: 24
    },
    {
        span: 8,
        label: "24.5",
        value: 24.5
    },
    {
        span: 8,
        label: "25",
        value: 25
    },
    {
        span: 8,
        label: "25.5",
        value: 25.5
    },
    {
        span: 8,
        label: "26",
        value: 26
    },
    {
        span: 8,
        label: "26.5",
        value: 26.5
    },
    {
        span: 8,
        label: "27",
        value: 27
    },
    {
        span: 8,
        label: "27.5",
        value: 27.5
    },
    {
        span: 8,
        label: "28",
        value: 28
    },
    {
        span: 8,
        label: "28.5",
        value: 28.5
    },
    {
        span: 8,
        label: "29",
        value: 29
    },
    {
        span: 8,
        label: "29.5",
        value: 29.5
    },
    {
        span: 8,
        label: "30",
        value: 30
    },
    {
        span: 8,
        label: "30.5",
        value: 30.5
    },
    {
        span: 8,
        label: "31",
        value: 31
    },
    {
        span: 8,
        label: "31.5",
        value: 31.5
    },
    {
        span: 8,
        label: "32",
        value: 32
    },
    {
        span: 8,
        label: "32.5",
        value: 32.5
    },
    {
        span: 8,
        label: "33",
        value: 33
    },
    {
        span: 8,
        label: "33.5",
        value: 33.5
    },
    {
        span: 8,
        label: "34",
        value: 34
    },
    {
        span: 8,
        label: "34.5",
        value: 34.5
    },
    {
        span: 8,
        label: "35",
        value: 35
    },
    {
        span: 8,
        label: "35.5",
        value: 35.5
    },
    {
        span: 8,
        label: "36",
        value: 36
    },
    {
        span: 8,
        label: "36.5",
        value: 36.5
    },
    {
        span: 8,
        label: "37",
        value: 37
    },
    {
        span: 8,
        label: "37.5",
        value: 37.5
    },
    {
        span: 8,
        label: "38",
        value: 38
    },
    {
        span: 8,
        label: "38.5",
        value: 38.5
    },
    {
        span: 8,
        label: "39",
        value: 39
    },
    {
        span: 8,
        label: "39.5",
        value: 39.5
    },
    {
        span: 8,
        label: "40",
        value: 40
    },
    {
        span: 8,
        label: "40.5",
        value: 40.5
    },
    {
        span: 8,
        label: "41",
        value: 41
    },
    {
        span: 8,
        label: "41.5",
        value: 41.5
    },
    {
        span: 8,
        label: "42",
        value: 42
    },
    {
        span: 8,
        label: "42.5",
        value: 42.5
    },
    {
        span: 8,
        label: "43",
        value: 43
    },
    {
        span: 8,
        label: "43.5",
        value: 43.5
    },
    {
        span: 8,
        label: "44",
        value: 44
    },
    {
        span: 8,
        label: "44.5",
        value: 44.5
    },
    {
        span: 8,
        label: "45",
        value: 45
    },
    {
        span: 8,
        label: "45.5",
        value: 45.5
    },
    {
        span: 8,
        label: "46",
        value: 46
    },
    {
        span: 8,
        label: "46.5",
        value: 46.5
    },
    {
        span: 8,
        label: "47.5",
        value: 47.5
    },
    {
        span: 8,
        label: "48.5",
        value: 48.5
    }
];

export const server = {
    apiKey: "HGD87yU2YTSGc56b",
    nodes: [
        {
            host: process.env.API_HOST,
            port: 8108,
            protocol: process.env.API_PROTOCOL,
        },
    ],

    // cacheSearchResultsForSeconds: 2 * 60, // Cache search results from server. Defaults to 2 minutes. Set to 0 to disable caching.
}

export const DEFAULT_MOBILE_WINDOW_SIZE = 576;
export const DEFAULT_DESKTOP_WINDOW_SIZE = 768;